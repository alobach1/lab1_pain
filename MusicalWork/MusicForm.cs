﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MusicalWork
{
    public partial class MusicForm : Form
    {
        public MusicForm()
        {
            InitializeComponent();
            MusicType = categoryControl1.Type;
        }

        private Music music;
        private List<Music> musics;
        public string MusicTitle
        {
            get;
            set;
        }

        public string MusicAuthor
        {
            get;
            set;
        }

        public DateTime MusicDate
        {
            get { return dateTimePicker1.Value; }
        }

        public Category MusicType
        {
            get;
            set;
        }

        public MusicForm(Music music, List<Music> musics)
        {
            InitializeComponent();
            this.music = music;
            this.musics = musics;
            
            if (music != null)
            {
                titleTextBox.Text = music.Title;
                authorTextBox.Text = music.Author;
                dateTimePicker1.Value = music.Date;
                categoryControl1.Type = music.Type;
               
            }
 
        }

     
        private void okButton_Click(object sender, EventArgs e)
        {
            if (ValidateChildren())
            {
                DialogResult = DialogResult.OK;
            }
            
        }

        private void cancelButton_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
        }

        private void tableLayoutPanel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void categoryControl1_Load(object sender, EventArgs e)
        {
            MusicType = categoryControl1.Type;
        }

        private void titleTextBox_Validated(object sender, EventArgs e)
        {
            MusicTitle = titleTextBox.Text;
            errorTitle.Clear();
        }

        private void titleTextBox_Validating(object sender, CancelEventArgs e)
        {
            if (titleTextBox.Text.Length < 4)
            {
                errorTitle.SetError(titleTextBox, "Music title is too short or empty");
                e.Cancel = true;
            }
        }

        private void authorTextBox_Validated(object sender, EventArgs e)
        {
            MusicAuthor = authorTextBox.Text;
            errorAuthor.Clear();
        }

        private void authorTextBox_Validating(object sender, CancelEventArgs e)
        {
            if (authorTextBox.Text.Length < 4)
            {
                errorAuthor.SetError(authorTextBox, "Music author is too short or empty");
                e.Cancel = true;
            }
        }

        private void categoryControl1_Click(object sender, EventArgs e)
        {
            MusicType = categoryControl1.Type;
        }

       
    }
}
