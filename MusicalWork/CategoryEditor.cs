﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing.Design;
using System.Windows.Forms.Design;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace MusicalWork
{
    [System.Security.Permissions.PermissionSet(System.Security.Permissions.SecurityAction.Demand, Name = "FullTrust")]
    public class CategoryEditor : System.Drawing.Design.UITypeEditor
    {
        public CategoryEditor()
        {

        }

        public override System.Drawing.Design.UITypeEditorEditStyle GetEditStyle(System.ComponentModel.ITypeDescriptorContext context)
        {
            return UITypeEditorEditStyle.DropDown;
        }

        // Displays the UI for value selection.
        public override object EditValue(System.ComponentModel.ITypeDescriptorContext context, System.IServiceProvider provider, object value)
        {
            IWindowsFormsEditorService edSvc =
            (IWindowsFormsEditorService)provider.GetService(typeof(IWindowsFormsEditorService));
            if (edSvc != null)
            {
                CategoryControl categoryControl  = new CategoryControl();
                categoryControl.Type = (Category)value;
                edSvc.DropDownControl(categoryControl);
                return categoryControl.Type;
            }
            return value;
        }

        public override bool GetPaintValueSupported(System.ComponentModel.ITypeDescriptorContext context)
        {
            return false;
        }




        public class CategoryControl : System.Windows.Forms.UserControl
        {
            private Category type;
            [BrowsableAttribute(true)]
            [EditorAttribute(typeof(CategoryEditor), typeof(System.Drawing.Design.UITypeEditor))]

            public Category Type
            {
                get
                {
                    return type;
                }
                set
                {
                    type = value;
                    Invalidate();
                }
            }

            private Image rock, rap, pop;


            public CategoryControl() : this(0)
            {
            }
            public CategoryControl(Category type)
            {
                rock = Image.FromFile(@"C:\Users\Anna\source\repos\MusicalWork\rock.jpg");
                rap = Image.FromFile(@"C:\Users\Anna\source\repos\MusicalWork\rap.jpg");
                pop = Image.FromFile(@"C:\Users\Anna\source\repos\MusicalWork\pop.jpeg");
                Type = Category.rock;
                this.Type = type;
                this.SetStyle(ControlStyles.AllPaintingInWmPaint, true);
            }

            protected override void OnPaint(PaintEventArgs e)
            {
                Graphics g = e.Graphics;

                g.FillRectangle(new SolidBrush(Parent.BackColor), e.ClipRectangle);
                Image img = rock;

                switch (Type)
                {
                    case Category.rock:
                        img = rock;
                        break;

                    case Category.rap:
                        img = rap;
                        break;

                    case Category.pop:
                        img = pop;
                        break;
                }

                g.DrawImage(img, e.ClipRectangle);
                base.OnPaint(e);
            }

            protected override void OnClick(EventArgs e)
            {
                switch (Type)
                {
                    case Category.pop:
                        Type = Category.rock;
                        break;

                    case Category.rock:
                        Type = Category.rap;
                        break;

                    case Category.rap:
                        Type = Category.pop;
                        break;
                }
                this.Refresh();
                base.OnClick(e);
            }
        }
    }
}
