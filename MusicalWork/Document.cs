﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MusicalWork
{
    public class Document
    {
        public List<Music> musics = new List<Music>();
        public event Action<Music> AddMusicEvent;
        public event Action<Music> UpdateMusicEvent;
        public event Action<Music> DeleteMusicEvent;

        public void AddMusic(Music music)
        {
            musics.Add(music);
            AddMusicEvent?.Invoke(music);
        }

        public void UpdateMusic(Music music)
        {
            UpdateMusicEvent?.Invoke(music);
        }




        public int getMusicCount()
        {
            return musics.Count;
        }

        public Music getMusicByIndex(int i)
        {
            return musics[i];
        }


        public void DeleteMusic(Music music)
        {
            musics.Remove(music);
            DeleteMusicEvent?.Invoke(music);
        }

    }
}
