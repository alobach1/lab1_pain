﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
public enum Category
{
    rock,
    pop,
    rap
}

namespace MusicalWork
{
    public class Music
    {
        public string Title
        {
            get;
            set;
        }
        public string Author
        {
            get;
            set;
        }
        public DateTime Date
        {
            get;
            set;
        }

        public Category Type
        {
            get;
            set;
        }
        public Music(string title, string author, DateTime date, Category type)
        {
            Title = title;
            Author = author;
            Date = date;
            Type = type;
        }
    }
}
