﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MusicalWork
{
    public partial class ParentForm : Form
    {
        public List<ChildForm> childrenList;
        Document document = new Document();

        public ParentForm()
        {
            InitializeComponent();
            childrenList = new List<ChildForm>();
        }

        private void newToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var newMDIChild = new ChildForm(document);
            // Set the Parent Form of the Child window.
            newMDIChild.MdiParent = this;
            // Adding to list of children
            childrenList.Add(newMDIChild);
            // Display the new form
            newMDIChild.Show();
        }

        private void ParentForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            e.Cancel = false;
        }
    }
}
