﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

internal enum Filtering { before, after, all }

namespace MusicalWork
{
    public partial class ChildForm : Form
    {
        private Filtering filtering;
        private Document Documents
        {
            get;
            set;
        }

        public ChildForm(Document document)
        {
            InitializeComponent();
            Documents = document;
            filtering = global::Filtering.all;
        }

        private void ChildForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            var parent = (ParentForm)this.MdiParent;
            if (parent.childrenList.Count == 1)
            {
                e.Cancel = true;

            }
            else
            {
                e.Cancel = false;
                parent.childrenList.Remove(this);

            }
        }

        private void ChildForm_Load(object sender, EventArgs e)
        {
            UpdateItems();
            Documents.AddMusicEvent += Document_AddMusicEvent;
            Documents.UpdateMusicEvent += Document_UpdateMusicEvent;
            Documents.DeleteMusicEvent += Document_DeleteMusicEvent;
        }

        private void Document_AddMusicEvent(Music music)
        {
            if ((filtering == global::Filtering.before && DateTime.Parse(music.Date.ToString()).Year < 2000) || (filtering == global::Filtering.after && DateTime.Parse(music.Date.ToString()).Year >= 2000) || (filtering == global::Filtering.all))
            {
                ListViewItem item = new ListViewItem();
                item.Tag = music;
                UpdateItem(item);
                musicListView.Items.Add(item);
                value.Text = musicListView.Items.Count.ToString();
            }

        }

        private void Document_UpdateMusicEvent(Music music)
        {
            var founded = false;
            ListViewItem item = null;
            foreach (ListViewItem it in musicListView.Items)
            {
                if (it.Tag == music)
                {
                    item = it;
                    founded = true;
                }
            }
            if (founded)
            {
                if ((filtering == global::Filtering.before && DateTime.Parse(music.Date.ToString()).Year < 2000) || (filtering == global::Filtering.after && DateTime.Parse(music.Date.ToString()).Year >= 2000) || (filtering == global::Filtering.all))
                {
                    UpdateItem(item);
                }
                else
                {
                    musicListView.Items.Remove(item);
                    value.Text = musicListView.Items.Count.ToString();
                }
            }
            else
            {
                if ((filtering == global::Filtering.before && DateTime.Parse(music.Date.ToString()).Year < 2000) || (filtering == global::Filtering.after && DateTime.Parse(music.Date.ToString()).Year >= 2000))
                {
                    ListViewItem i = new ListViewItem();
                    i.Tag = music;
                    UpdateItem(i);
                    musicListView.Items.Add(i);
                    value.Text = musicListView.Items.Count.ToString();
                }
            }

        }


        private void Document_DeleteMusicEvent(Music music)
        {
            if ((filtering == global::Filtering.before && DateTime.Parse(music.Date.ToString()).Year < 2000) || (filtering == global::Filtering.after && DateTime.Parse(music.Date.ToString()).Year >= 2000) || (filtering == global::Filtering.all))
            {
                foreach (ListViewItem item in musicListView.Items)
                {
                    if (item.Tag == music)
                    {
                        musicListView.Items.Remove(item);
                        value.Text = musicListView.Items.Count.ToString();
                    }
                }
            }

        }

        private void UpdateItem(ListViewItem item)
        {

            Music music = (Music)item.Tag;
            while (item.SubItems.Count < 4)
            {
                item.SubItems.Add(new ListViewItem.ListViewSubItem());
            }

            item.SubItems[0].Text = music.Title;
            item.SubItems[1].Text = music.Author;
            item.SubItems[2].Text = music.Date.ToShortDateString();
            item.SubItems[3].Text = music.Type.ToString();
        }

        private void UpdateItems()
        {
            musicListView.Items.Clear();
            foreach (Music music in Documents.musics)
            {
                if ((filtering == global::Filtering.before && DateTime.Parse(music.Date.ToString()).Year < 2000) || (filtering == global::Filtering.after && DateTime.Parse(music.Date.ToString()).Year >= 2000) || (filtering == global::Filtering.all))
                {

                var item = new ListViewItem();
                item.Tag = music;
                UpdateItem(item);
                musicListView.Items.Add(item);
                }
            }
            value.Text = musicListView.Items.Count.ToString();
        }

        private void createToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MusicForm musicForm = new MusicForm(null, Documents.musics);
            if (musicForm.ShowDialog() == DialogResult.OK)
            {
                Music newMusic = new Music(musicForm.MusicTitle, musicForm.MusicAuthor, musicForm.MusicDate, musicForm.MusicType);

                Documents.AddMusic(newMusic);

            }
            value.Text = musicListView.Items.Count.ToString();
        }

        private void editToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (musicListView.SelectedItems.Count == 1)
            {
                Music music = (Music)musicListView.SelectedItems[0].Tag;
                MusicForm musicForm = new MusicForm(music, Documents.musics);
                if (musicForm.ShowDialog() == DialogResult.OK)
                {
                    music.Title = musicForm.MusicTitle;
                    music.Author = musicForm.MusicAuthor;
                    music.Date = musicForm.MusicDate;
                    music.Type = musicForm.MusicType;

                    Documents.UpdateMusic(music);
                    value.Text = musicListView.Items.Count.ToString();
                }
            }

        }

        private void delteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (musicListView.SelectedItems.Count == 1)
            {
                var deleteList = (from ListViewItem x in musicListView.SelectedItems select (Music)x.Tag).ToList();

                foreach (var x in deleteList)
                {
                    Documents.DeleteMusic(x);
                }
                value.Text = musicListView.Items.Count.ToString();
            }

        }

        private void ChildForm_Activated(object sender, EventArgs e)
        {
           ToolStripManager.Merge(statusStripChild, ((ParentForm)MdiParent).statusStripParent);
        }

        private void ChildForm_Deactivate(object sender, EventArgs e)
        {
            ToolStripManager.RevertMerge(((ParentForm)MdiParent).statusStripParent, statusStripChild);
        }

        private void after2000ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            filtering = global::Filtering.after;
            UpdateItems();
        }

        private void allToolStripMenuItem_Click(object sender, EventArgs e)
        {
            filtering = global::Filtering.all;
            UpdateItems();
        }

        private void before2000ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            filtering = global::Filtering.before;
            UpdateItems();
        }
    }
}

